var Webpack = require("webpack");
var HtmlPlugin = require("html-webpack-plugin");
var CleanPlugin = require("clean-webpack-plugin");
var SplitByPathPlugin = require("webpack-split-by-path");
var path = require("path");

var OUTPUT_DIR = "./dist";
var NODE_MODULES_DIR = "./node_modules";
var PORT = process.env.PORT || 8080;

module.exports = {
  entry: {
    app: [
      "webpack-dev-server/client?http://localhost:" + PORT,
      "webpack/hot/only-dev-server",
      "./src/index.jsx"
    ]
  },
  output: {
    path: OUTPUT_DIR,
    filename: "[name].js",
    chunkFilename: "chunk-[id]-[chunkhash:8].js"
  },
  resolve: {
    extensions: ["", ".js", ".jsx"]
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loaders: ["babel"],
      exclude: /\/node_modules\//
    }, {
      test: /\.css$/,
      loaders: ["style", "css"]
    }, {
      test: require.resolve("react"),
      loader: "expose?React"
    },
    // Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
    // loads bootstrap's css.
    {
      test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&minetype=application/font-woff"
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&minetype=application/octet-stream"
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file"
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&minetype=image/svg+xml"
    }]
  },
  devServer: {
    contentBase: OUTPUT_DIR,
    devTool: "eval",
    progress: true,
    colors: true,
    hot: true,
    port: PORT
  },
  plugins: [
    new CleanPlugin([OUTPUT_DIR]),
    new SplitByPathPlugin([{ name: "lib", path: path.join(__dirname, "node_modules") }]),
    new Webpack.HotModuleReplacementPlugin(),
    new HtmlPlugin({
      title: "Zipwhip Shell",
      inject: "body",
      template: "./src/index.html.tpl"
    }),
    new Webpack.NoErrorsPlugin()
  ]
};
