import { Component, PropTypes } from "react";
import Panel from "react-bootstrap/lib/Panel";
import Button from "react-bootstrap/lib/Button";

export default class Login extends Component {

  handleLogin() {
    console.log("LOGGED IN!");
    alert("logged in !");
    this.context.history.replace("/");
  }

  render() {
    return <Panel>
      <h4>login</h4>
      <Button onClick={() => this.handleLogin()}>Log in</Button>
    </Panel>
  }
}

Login.contextTypes = {
  history: PropTypes.object
}
