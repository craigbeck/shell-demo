module.exports = {
  path: "plugins",

  getComponents(location, cb) {
    console.log("Plugins.getComponents");
    require.ensure([], require => {
      var Plugins = require("./components/Plugins");
      console.log("Plugins", Plugins);
      cb(null, Plugins)
    });
  }
}
