module.exports = {
  path: "/settings",

  getChildRoutes(location, cb) {
    require.ensure([], require => {
      cb(null, [
        require("./routes/Plugins")
      ])
    })
  },

  getComponents(location, cb) {
    console.log("Settings.getComponents");
    require.ensure([], require => {
      cb(null, require("./components/Settings"))
    });
  }
}
