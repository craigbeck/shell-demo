import { Component } from "react";
import { Link } from "react-router";

export default class Settings extends Component {
  render() {
    console.log("Settings.render", this.props, this.context);
    if (this.props.children) {
      return this.props.children;
    }
    return <div>
      <h4>settings</h4>
      <Link to="/settings/plugins">plugins</Link>
    </div>
  }
}
