import { Component } from "react";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import Navbar from "react-bootstrap/lib/Navbar";
import Nav from "react-bootstrap/lib/Nav";

import { Link } from "react-router";

class DefaultView extends Component {
  render() {
    return <Row>
      <Col xs={12}>
        <p>This is the default app view</p>
        <ul>
          <li>
            <Link to="/login">go login</Link>
          </li>
          <li>
            <Link to="/settings">settings</Link>
          </li>
        </ul>
      </Col>
    </Row>;
  }
}

export default class AppShell extends Component {
  render() {
    const fluid = false;
    return <div>
      <Navbar static fixed>
        <Navbar.Brand><a href="/">Shell 1.3</a></Navbar.Brand>
      </Navbar>
      <Grid fluid={fluid}>
        {this.props.children || <DefaultView/>}
      </Grid>
    </div>
  }
}
