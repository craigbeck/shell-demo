import React from "react";
import ReactDOM from "react-dom";
import createHistory from 'history/lib/createHashHistory'
import { Router } from 'react-router'


require("bootstrap/dist/css/bootstrap.min.css");

const rootRoute = {
  component: "div",
  childRoutes: [{
    path: "/",
    component: require("./containers/AppShell"),
    childRoutes: [
      require("./routes/Login"),
      require("./routes/Settings")
    ]
  }]
};

const history = createHistory({ queryKey: false });

ReactDOM.render(
  <Router history={history} routes={rootRoute} />,
  document.getElementById("app")
);
